# Salesforce Canvas Sample Implementation

This repository shows how to integrate a C# Web Forms application in Salesforce using Canvas.

*For more detailed information regarding Canvas see Salesforce's [Canvas Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.platform_connect.meta/platform_connect/canvas_framework_intro.htm)*

## Repository Overview

<<<<<<< HEAD
The solution has two projects:

* SfdcCanvas
  * [CanvasRequest.cs](https://bitbucket.org/JosephAllenRepos/sfdc-canvas/src/master/SfdcCanvas/CanvasRequest.cs) *(Model Object for deserializing the Salesforce [CanvasRequest](https://developer.salesforce.com/docs/atlas.en-us.platform_connect.meta/platform_connect/canvas_request_object.htm))*
  * [OAuth.cs](https://bitbucket.org/JosephAllenRepos/sfdc-canvas/src/master/SfdcCanvas/OAuth.cs) *(Controller for decrypting the Canvas encryptedSignedRequest)*
* CanvasWebFormsApp
  * Demonstrates an example pattern for creating a C# project
  * Shows how to execute a query using the OAuth token returned by the CanvasRequest JSON
=======
* Demonstrates an example pattern for creating a C# project
* Shows how to execute a query using the OAuth token returned by the [CanvasRequest](https://developer.salesforce.com/docs/atlas.en-us.platform_connect.meta/platform_connect/canvas_request_object.htm) JSON

## Folder Structure

* SFDC - *Includes handlers and objects for implementing Canvas integration*
    * OAuth.cs - *Handles decrypting Canvas signed request and deserialzing into a CanvasRequest object*
    * Query.cs - *Executes a query against the rest endpoint defined in the canvasRequest.context.links.queryUrl field*
        * Models - *folder for various model object sent to and returnd from Saleforce*
            * CanvasRequest.cs - *Object for holding deserialzed Canvas Request*
* Utils - *folder for utilities classes*
    * JSON.cs - *Reusable methods for serializng and deserializng JSON data. Mimics the Saleforce [JSON Class](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_class_System_Json.htm)*
>>>>>>> 21ab7de4de825d4981a21236c232bf996a180878

## Development Environment Setup

* Visual Studio 2015
* Certificate on the Developer Machine. A Self Signed certificate will work with Salesforce

## Salesforce Setup

## Running and Testing