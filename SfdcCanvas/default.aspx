﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SfdcCanvas._default" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>.Net Canvas Consumer</title>
</head>
<body>
    <h2>.Net Canvas Consumer</h2>
    <ul>
        <li><%=Greeting%></li>
        <li>Signed Request Status:
            <div>
            <%=SignedRequestStatus%>
            </div>
        </li>
    </ul>
</body>
</html>
