﻿using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;

namespace SfdcCanvas.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public class JSON
    {
        /// <summary>
        /// Deserializes the specified JSON string into an C# object of the specified type.
        /// </summary>
        /// <typeparam name="T">The C# type of the object that this method creates after deserializing the JSON content.</typeparam>
        /// <param name="jsonString">The JSON content to deserialize.</param>
        /// <returns>C# Object</returns>
        public static T Deserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            ms.Close();
            return obj;
        }

        /// <summary>
        /// Serializes C# objects into JSON content.
        /// </summary>
        /// <typeparam name="T">C# Object Type</typeparam>
        /// <param name="objectToSerialize">The C# object to serialize.</param>
        /// <returns>JSON string representing the serailzed C# Objects</returns>
        public static string Serialize<T>(T objectToSerialize)
        {
            MemoryStream ms = new MemoryStream();
            DataContractJsonSerializer ds = new DataContractJsonSerializer(typeof(T));
            DataContractJsonSerializerSettings s = new DataContractJsonSerializerSettings();
            ds.WriteObject(ms, objectToSerialize);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }
    }
}
