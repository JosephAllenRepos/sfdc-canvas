﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SfdcCanvas.SFDC.Models
{
    /// <summary>
    /// Model object for holding deserialzed Canvas Request
    /// </summary>
    [DataContract]
    public class CanvasRequest
    {
        [DataMember]
        public string algorithm { get; set; }
        [DataMember]
        public int issuedAt { get; set; }
        [DataMember]
        public string userId { get; set; }
        [DataMember]
        public Client client { get; set; }
        [DataMember]
        public Context context { get; set; }
    }

    [DataContract]
    public class Client
    {
        [DataMember]
        public object refreshToken { get; set; }
        [DataMember]
        public string instanceId { get; set; }
        [DataMember]
        public string targetOrigin { get; set; }
        [DataMember]
        public string instanceUrl { get; set; }
        [DataMember]
        public string oauthToken { get; set; }
    }

    [DataContract]
    public class User
    {
        [DataMember]
        public string userId { get; set; }
        [DataMember]
        public string userName { get; set; }
        [DataMember]
        public string firstName { get; set; }
        [DataMember]
        public string lastName { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string fullName { get; set; }
        [DataMember]
        public string locale { get; set; }
        [DataMember]
        public string language { get; set; }
        [DataMember]
        public string timeZone { get; set; }
        [DataMember]
        public string profileId { get; set; }
        [DataMember]
        public string roleId { get; set; }
        [DataMember]
        public string userType { get; set; }
        [DataMember]
        public string currencyISOCode { get; set; }
        [DataMember]
        public string profilePhotoUrl { get; set; }
        [DataMember]
        public string profileThumbnailUrl { get; set; }
        [DataMember]
        public string siteUrl { get; set; }
        [DataMember]
        public string siteUrlPrefix { get; set; }
        [DataMember]
        public string networkId { get; set; }
        [DataMember]
        public bool accessibilityModeEnabled { get; set; }
        [DataMember]
        public bool isDefaultNetwork { get; set; }
    }

    [DataContract]
    public class Links
    {
        [DataMember]
        public string loginUrl { get; set; }
        [DataMember]
        public string enterpriseUrl { get; set; }
        [DataMember]
        public string metadataUrl { get; set; }
        [DataMember]
        public string partnerUrl { get; set; }
        [DataMember]
        public string restUrl { get; set; }
        [DataMember]
        public string sobjectUrl { get; set; }
        [DataMember]
        public string searchUrl { get; set; }
        [DataMember]
        public string queryUrl { get; set; }
        [DataMember]
        public string recentItemsUrl { get; set; }
        [DataMember]
        public string chatterFeedsUrl { get; set; }
        [DataMember]
        public string chatterGroupsUrl { get; set; }
        [DataMember]
        public string chatterUsersUrl { get; set; }
        [DataMember]
        public string chatterFeedItemsUrl { get; set; }
        [DataMember]
        public string userUrl { get; set; }
    }

    [DataContract]
    public class Application
    {
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string canvasUrl { get; set; }
        [DataMember]
        public string applicationId { get; set; }
        [DataMember]
        public string version { get; set; }
        [DataMember]
        public string authType { get; set; }
        [DataMember]
        public string referenceId { get; set; }
        [DataMember]
        public List<object> options { get; set; }
        [DataMember]
        public string samlInitiationMethod { get; set; }
        [DataMember]
        public bool isInstalledPersonalApp { get; set; }
        [DataMember]
        public string @namespace { get; set; }
        [DataMember]
        public string developerName { get; set; }
    }

    [DataContract]
    public class Organization
    {
        [DataMember]
        public string organizationId { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public bool multicurrencyEnabled { get; set; }
        [DataMember]
        public object namespacePrefix { get; set; }
        [DataMember]
        public string currencyIsoCode { get; set; }
    }

    [DataContract]
    public class Dimensions
    {
        [DataMember]
        public string width { get; set; }
        [DataMember]
        public string height { get; set; }
        [DataMember]
        public string maxWidth { get; set; }
        [DataMember]
        public string maxHeight { get; set; }
        [DataMember]
        public string clientWidth { get; set; }
        [DataMember]
        public string clientHeight { get; set; }
    }

    [DataContract]
    public class Parameters
    {
        [DataMember]
        public string sessionID { get; set; }
        [DataMember]
        public string sapBillTo { get; set; }
        [DataMember]
        public object dealerNumber { get; set; }
        [DataMember]
        public string accountType { get; set; }
        [DataMember]
        public List<string> permissions { get; set; }
        [DataMember]
        public string libertiesNumber { get; set; }
        [DataMember]
        public string navAccountId { get; set; }
        [DataMember]
        public string navContactId { get; set; }
        [DataMember]
        public string relayState { get; set; }
    }

    [DataContract]
    public class Record
    {
    }

    [DataContract]
    public class Version
    {
        [DataMember]
        public string season { get; set; }
        [DataMember]
        public string api { get; set; }
    }

    [DataContract]
    public class Environment
    {
        [DataMember]
        public object referer { get; set; }
        [DataMember]
        public string locationUrl { get; set; }
        [DataMember]
        public string displayLocation { get; set; }
        [DataMember]
        public object sublocation { get; set; }
        [DataMember]
        public string uiTheme { get; set; }
        [DataMember]
        public Dimensions dimensions { get; set; }
        [DataMember]
        public Parameters parameters { get; set; }
        [DataMember]
        public Record record { get; set; }
        [DataMember]
        public Version version { get; set; }
    }

    [DataContract]
    public class Context
    {
        [DataMember]
        public User user { get; set; }
        [DataMember]
        public Links links { get; set; }
        [DataMember]
        public Application application { get; set; }
        [DataMember]
        public Organization organization { get; set; }
        [DataMember]
        public Environment environment { get; set; }
    }
}
