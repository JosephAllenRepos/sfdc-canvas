﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SfdcCanvas.SFDC
{
    /// <summary>
    /// Handles decrypting Canvas signed request and deserialzing into a CanvasRequest object
    /// </summary>
    public class OAuth
    {

        /// <summary>
        /// Immediately after your object is instantiated, check this property to see if the signed 
        /// request request is authentic. If it's false then the request could be a forgery.
        /// </summary>
        public bool IsAuthenticatedCanvasUser { get; private set; }

        /// <summary>Use CanvasContextObject from your codebehind/server-side ASP.Net code.</summary>
        public Models.CanvasRequest CanvasRequest { get; private set; }

        /// <summary>
        /// Use CanvasContextJson for your client-side JavaScript code. This is the JSON serialization of
        /// the CanvasContextObject property (type:RootObject). For example, you can stash it in a 
        /// hidden field and later deserialize in document.ready: var sr = JSON.parse($('#hiddenfield').val());
        /// </summary>
        public string CanvasRequestJson { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerSecret">Our app and SalesForce both know the consumer secret.</param>
        /// <param name="encryptedSignedRequest">This request comes from SalesForce. We verify it using 
        /// our consumer secret.</param>
        public OAuth(string consumerSecret, string encryptedSignedRequest)
        {
            CanvasRequest = GetCanvasRequest(consumerSecret, encryptedSignedRequest);
            CanvasRequestJson = Utils.JSON.Serialize(CanvasRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerSecret"></param>
        /// <param name="signedRequest"></param>
        /// <returns>The CanvasRequest is what we pass via JSON to the Salesforce Canvas client (JavaScript)</returns>
        private Models.CanvasRequest GetCanvasRequest(string consumerSecret, string signedRequest)
        {
            string[] signedRequestLst = signedRequest.Split('.');
            string encodedSignature = signedRequestLst[0];
            string requestPlusToken = signedRequestLst[1];

            IsAuthenticatedCanvasUser = isSignedRequestValid(consumerSecret, encodedSignature, requestPlusToken);

            byte[] requestPlusTokenBytes = Convert.FromBase64String(requestPlusToken);
            Models.CanvasRequest request = Utils.JSON.Deserialize<Models.CanvasRequest>(Encoding.UTF8.GetString(requestPlusTokenBytes));
            return request;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerSecret">Consumer Secret from the Related Salesforce Connected App</param>
        /// <param name="encodedSignature"></param>
        /// <param name="requestPlusToken"></param>
        /// <returns></returns>
        private bool isSignedRequestValid(string consumerSecret, string encodedSignature, string requestPlusToken)
        {
            byte[] decodedSig = Convert.FromBase64String(encodedSignature);

            var encoding = new ASCIIEncoding();
            var hmacSha256 = new HMACSHA256(encoding.GetBytes(consumerSecret));
            byte[] hashMessage = hmacSha256.ComputeHash(encoding.GetBytes(requestPlusToken));

            return decodedSig.SequenceEqual(hashMessage); // true => we can trust; false otherwise
        }
    }
}
