﻿using System;
using System.Net;

namespace SfdcCanvas.SFDC
{
    /// <summary>
    /// Class representing the Salesforce REST Query endpoint
    /// </summary>
    public class Query
    {
        /// <summary>
        /// Executes a query against the rest endpoint defined in the canvasRequest.context.links.queryUrl field
        /// </summary>
        /// <param name="canvasRequest">Canvas Request object that hold the various paramters for OAuth and Endpoint</param>
        /// <param name="soql">SOQL Select statement used to query salesforce</param>
        /// <returns></returns>
        public static string Execute(SFDC.Models.CanvasRequest canvasRequest, String soql)
        {
            string url = canvasRequest.client.instanceUrl + canvasRequest.context.links.queryUrl + "?q=";
            url += Uri.EscapeUriString(soql);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            WebClient wClient = new WebClient();
            wClient.Headers.Add("Authorization", "Bearer " + canvasRequest.client.oauthToken);
            string wResponse = wClient.DownloadString(url);

            return wResponse;
        }
    }
}
